import matplotlib.pyplot as plt
from simple_pid import PID


class WaterBoiler:
    """
    Simple simulation of a water boiler which can heat up water
    and where the heat dissipates slowly over time

    Changed it so that it runs in computation time not real time (10 seconds
    per run)
    """

    def __init__(self):
        self.water_temp = 20

    def update(self, boiler_power, dt):
        if boiler_power > 0:
            # Boiler can only produce heat, not cold
            self.water_temp += 1 * boiler_power * dt

        # Some heat dissipation
        self.water_temp -= 0.02 * dt
        return self.water_temp


if __name__ == '__main__':
    boiler = WaterBoiler()
    water_temp = boiler.water_temp

    pid = PID(5, 0.01, 0.1, setpoint=water_temp)
    pid.output_limits = (0, 100)

    # Keep track of values for plotting
    setpoint, y, x = [], [], []
    dt = 0.1
    i = 0
    start_time = 0
    while i < 10:

        power = pid(water_temp, dt)
        water_temp = boiler.update(power, dt)

        x += [start_time + i]
        y += [water_temp]
        setpoint += [pid.setpoint]

        if i > 1:
            pid.setpoint = 100

        i += dt
        print(i)

    plt.plot(x, y, label='measured')
    plt.plot(x, setpoint, label='target')
    plt.xlabel('time')
    plt.ylabel('temperature')
    plt.legend()
    plt.show()
