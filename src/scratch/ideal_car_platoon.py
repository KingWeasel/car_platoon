from numba import jit
import numpy as np
from PyQt6.QtGui import QPen
from PyQt6.QtWidgets import QMainWindow
import pyqtgraph as pg

# constants
TAU = 0.5  # System 'time lag' defined in the simulation
PLATOON_SIZE = 4
SAFE_DISTANCE = 2  # this is the same as D_min defined in the paper
CAR_LENGTH = 5
START_TIME = 0
STOP_TIME = 20
TIME_STEP = 0.1


@jit(nopython=True)
def ideal_car_acceleration(times: np.ndarray) -> np.ndarray:
    # Acceleration is 4 m/s^2 between 5 and 10 seconds
    return np.array([0 if (t <= 5 or t > 10) else 4 for t in times])


@jit(nopython=True)
def ideal_car_velocity(times: np.ndarray) -> np.ndarray:
    accelerations = ideal_car_acceleration(times)

    velocities = np.zeros(len(times))
    last_t = times[0]
    last_v = 0
    for i, (t, a) in enumerate(zip(times, accelerations)):
        # current velocity is a function of the time delta, current acceleration,
        # and last velocity
        velocities[i] = (t - last_t) * a + last_v
        last_t = t
        last_v = velocities[i]
    return velocities


@jit(nopython=True)
def ideal_car_position(times: np.ndarray, p0: float) -> np.ndarray:
    velocities = ideal_car_velocity(times)

    positions = np.zeros(len(times))
    first_t = times[0]
    last_t = first_t
    last_p = p0
    for i, (t, v) in enumerate(zip(times, velocities)):
        # current position is a function of the time delta, current velocity,
        # and last position
        positions[i] = ((t - last_t) * v) + last_p
        last_t = t
        last_p = positions[i]
    return positions


if __name__ == '__main__':
    # the paper went from t = 0..20 so I am too
    # num_times = int((STOP_TIME - START_TIME) / TIME_STEP)
    times = np.arange(START_TIME, STOP_TIME, TIME_STEP)

    accelerations = []
    velocities = []
    positions = []
    car_times = []

    # calculate dynamics for the entire platoon
    for i in range(PLATOON_SIZE):
        end = times[-1]
        tadd = 5 * i
        end_tadd = end + tadd
        num_steps = int((end_tadd - end) / TIME_STEP)

        # bump all new times up by i times the lag
        new_times = times + (TAU * i)

        # calc these using the original times because the acceleration logic
        # will hold then. Just move the initial positions back.
        # NOTE: FACTOR IN CAR LENGTH!!!
        car_acceleration = ideal_car_acceleration(times)
        car_velocity = ideal_car_velocity(times)
        car_position = ideal_car_position(times, -1 * i * SAFE_DISTANCE)

        accelerations.append(car_acceleration)
        velocities.append(car_velocity)
        positions.append(car_position)
        car_times.append(new_times)

    # plotting
    glw = pg.GraphicsLayoutWidget()

    colors = ('red', 'green', 'blue', 'yellow')
    lead_pen = pg.mkPen({'color': colors[0], 'width': 3, 'dash': (1, 2)})
    follow_pens: list[QPen] = [pg.mkPen({'color': colors[i + 1], 'width': 2})
                               for i in range(3)]

    # acceleration plot. Plot lead car with a different pen
    aplot: pg.PlotItem = glw.addPlot(title='Acceleration', x=car_times[0],
                                     y=accelerations[0], pen=lead_pen)
    [aplot.plot(x=t, y=a, pen=pen) for t, a, pen in
     zip(car_times[1:], accelerations[1:], follow_pens)]
    aplot.setLabel('left', 'Acceleration', units='m/s^2')
    aplot.setLabel('bottom', 'Time', units='sec')

    # make a legend on the top plot
    alegend = pg.LegendItem((80, 60), offset=(70, 20))
    curve_names = [f'Car {i}' for i in range(PLATOON_SIZE)]
    [alegend.addItem(curve, curve_name) for curve, curve_name
     in zip(aplot.curves, curve_names)]
    alegend.setParentItem(aplot)

    glw.nextRow()

    # velocity plot. Plot lead car with a different pen
    vplot: pg.PlotItem = glw.addPlot(title='Velocity', x=car_times[0],
                                     y=velocities[0], pen=lead_pen)
    [vplot.plot(x=t, y=v, pen=pen) for t, v, pen in
     zip(car_times[1:], velocities[1:], follow_pens)]
    vplot.setLabel('left', 'Velocity', units='m/s')
    vplot.setLabel('bottom', 'Time', units='sec')

    glw.nextRow()

    # positions plot. Plot lead car with a different pen
    pplot: pg.PlotItem = glw.addPlot(title='Position', x=car_times[0],
                                     y=positions[0], pen=lead_pen)
    [pplot.plot(x=t, y=p, pen=pen) for t, p, pen in
     zip(car_times[1:], positions[1:], follow_pens)]
    pplot.setLabel('left', 'Position', units='m')
    pplot.setLabel('bottom', 'Time', units='sec')

    win = QMainWindow()
    win.setWindowTitle('Ideal Case Plots')
    win.setCentralWidget(glw)
    win.showMaximized()
    win.setFocus()

    # start gui event loop so that plotting can happen
    pg.exec()
