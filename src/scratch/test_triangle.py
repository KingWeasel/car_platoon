import numpy as np
import pyqtgraph as pg


def triangle(x: np.ndarray, a: float, b: float, c: float) -> np.ndarray:
    return np.fmax(np.fmin((c - x) / (c - b), (x - a) / (b - a)), 0)


if __name__ == '__main__':
    x = np.array(range(100))
    mytriangle = triangle(x, 10, 40, 70)
    pg.plot(x=x, y=mytriangle)
    pg.exec()
