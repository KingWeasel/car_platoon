from PyQt6.QtWidgets import QMainWindow
import pyqtgraph as pg
from simple_pid import PID


class Car:
    """
    Simple simulation of a that speeds up to 20 m/s"""

    def __init__(self):
        self.speed = 0

    def update(self, acceleration, dt):
        if acceleration != 0:
            # this should handle negative acceleration
            self.speed += acceleration * dt
        return self.speed


if __name__ == '__main__':
    car = Car()
    speed = car.speed

    # defined in paper
    Kp = 2
    Ki = 3
    Kd = 0.5

    pid = PID(Kp=Kp, Ki=Ki, Kd=Kd, setpoint=0)
    pid.output_limits = (-5, 5)

    # Keep track of values for plotting
    setpoint, y, x, accels = [], [], [], []
    dt = 0.1
    i = 0
    start_time = 0

    while i < 100:
        accel = pid(speed, dt)
        speed = car.update(accel, dt)

        x += [start_time + i]
        y += [speed]
        accels += [accel]
        setpoint += [pid.setpoint]

        if 5 < i < 20:
            pid.setpoint = 20
        elif 20 < i < 40:
            pid.setpoint = 40
        elif i > 40:
            pid.setpoint = 0

        i += dt

    glw = pg.GraphicsLayoutWidget(show=True, title='Speed and Setpoint')
    aplot = glw.addPlot(x=x, y=y, title='Speed and Setpoint')
    alegend = pg.LegendItem(offset=(40, 40))
    mpen = pg.mkPen({'color': 'g', 'width': 2})
    m = aplot.plot(x, y, label='measured', pen=mpen)
    alegend.addItem(m, 'measured')
    tpen = pg.mkPen({'color': 'b', 'width': 2, 'dash': (1, 2)})
    t = aplot.plot(x, setpoint, label='target', pen=tpen)
    alegend.addItem(t, 'target')
    apen = pg.mkPen({'color': 'r', 'width': 2})
    a = aplot.plot(x, accels, label='acceleration', pen=apen)
    alegend.addItem(a, 'acceleration')
    aplot.setLabel('bottom', 'time', units='s')
    aplot.setLabel('left', 'speed', units='m/s')
    alegend.setParentItem(aplot)

    win = QMainWindow()
    win.setCentralWidget(glw)
    win.setWindowTitle('Learn PID')
    win.showMaximized()
    pg.exec()
