from numba import njit
import numpy as np
from PyQt6.QtGui import QPen
from PyQt6.QtWidgets import QMainWindow
import pyqtgraph as pg
from simple_pid import PID

# constants, using metric units
TIME_LAG = 0.5  # System 'time lag' Tau defined in the paper's simulation
PLATOON_SIZE = 4
SAFE_DISTANCE = 2  # this is the same as Dmin defined in the paper
TARGET_TIME_GAP = 3  # 3 seconds seems like a good time gap to keep from a leading car
CAR_LENGTH = 5  # rounded down from the length of my Frontier
# CAR_MASS = 2e6
# g = 9.8
# F_ROLLING = 0.01
START_TIME = 0  # the paper went from t = 0..20 but plotted until 50
# STOP_TIME = 20
STOP_TIME = 50
TIME_STEP = 0.1

# manual PID parameters defined in paper
Kp = 2
Ki = 3
Kd = 0.5


@njit
def lead_positions(times: np.ndarray, velocities: np.ndarray,
                   p0: float = 0) -> np.ndarray:
    positions = np.zeros(len(times))
    first_t = times[0]
    last_t = first_t
    last_p = p0
    for i, (t, v) in enumerate(zip(times, velocities)):
        # current position is a function of the time delta, current velocity,
        # and last position
        positions[i] = ((t - last_t) * v) + last_p
        last_t = t
        last_p = positions[i]
    return positions


@njit
def lead_velocities(times: np.ndarray, accelerations: np.ndarray) -> np.ndarray:
    velocities = np.zeros(len(times))
    last_t = times[0]
    last_v = 0
    for i, (t, a) in enumerate(zip(times, accelerations)):
        # current velocity is a function of the time delta,
        # current acceleration, and last velocity
        velocities[i] = (t - last_t) * a + last_v
        last_t = t
        last_v = velocities[i]
    return velocities


@njit
def lead_car_dynamics(times: np.ndarray) -> tuple[np.ndarray]:
    # the paper says the acceleration is 4 m/s^2 if t is between 5 and 10 seconds
    lead_a = np.array([4 if (5 < t <= 10) else 0 for t in times])
    lead_v = lead_velocities(times, lead_a)
    lead_p = lead_positions(times, lead_v)
    return lead_a, lead_v, lead_p


# can't use numba because of PID
def follower_dynamics(times: np.ndarray, lead_positions: np.ndarray,
                      lead_velocities: np.ndarray, car_index: int
                      ) -> tuple[np.ndarray]:

    # how many index positions to lag by to emulate a real time lag Tau
    index_lag = int(TIME_LAG / TIME_STEP)

    # car starts in sync with the car leading it with the correct time gap
    current_a = 0
    current_v = lead_velocities[index_lag]
    current_p = (lead_positions[index_lag] - CAR_LENGTH
                 - SAFE_DISTANCE - current_v * TARGET_TIME_GAP)

    len_times = len(times)

    # this function is going to return these after updating them based on
    # leading car dynamics
    follower_a = current_a * np.ones(len_times)
    follower_v = current_v * np.ones(len_times)
    follower_p = current_p * np.ones(len_times)
    follower_perror = np.zeros(len_times)

    pid = PID(Kp=Kp, Ki=Ki, Kd=Kd, setpoint=current_p)
    # pid.output_limits = (-5, 5)

    # we'll let position be the front of the car (matches paper).
    # So then, position of a follower
    # car should be front car positition - constant time gap - car length
    ilpp = index_lag
    for i, lp in enumerate(lead_positions[:-1 * index_lag], start=ilpp):

        target_p = lp - CAR_LENGTH - SAFE_DISTANCE - current_v * TARGET_TIME_GAP

        # the control part
        pid.setpoint = target_p
        current_a = pid(current_p, TIME_STEP)

        # collect values for return
        follower_perror[i] = target_p - follower_p[i - index_lag]
        follower_p[i] = current_p
        follower_a[i] = current_a
        current_v += current_a * TIME_STEP
        follower_v[i] = current_v
        current_p += current_v * TIME_STEP

    return follower_a, follower_v, follower_p, follower_perror


if __name__ == '__main__':
    times = np.arange(START_TIME, STOP_TIME, TIME_STEP)

    platoon_a = []
    platoon_v = []
    platoon_p = []
    platoon_perror = []
    platoon_verror = []

    # calculate dynamics for the entire platoon
    for i in range(PLATOON_SIZE):
        if not i:
            # lead car
            (a, v, p) = lead_car_dynamics(times)
            perror = None
        else:
            # a following car, feed in dynamics from car in front of it
            last_v = v
            (a, v, p, perror) = follower_dynamics(times, p, v, i)
            verror = last_v - v

        platoon_a.append(a)
        platoon_v.append(v)
        platoon_p.append(p)

        if perror is not None:
            platoon_perror.append(perror)
            platoon_verror.append(verror)

    # plotting
    glw = pg.GraphicsLayoutWidget()

    colors = ('red', 'green', 'blue', 'yellow')
    lead_pen = pg.mkPen({'color': colors[0], 'width': 3, 'dash': (1, 2)})
    follow_pens: list[QPen] = [pg.mkPen({'color': color, 'width': 2})
                               for color in colors[1:]]

    # acceleration plot. Plot lead car with a different pen
    aplot: pg.PlotItem = glw.addPlot(title='Acceleration', x=times,
                                     y=platoon_a[0], pen=lead_pen)
    [aplot.plot(x=times, y=a, pen=pen) for a, pen in
     zip(platoon_a[1:], follow_pens)]
    aplot.setLabel('left', 'Acceleration', units='m/s^2')
    aplot.setLabel('bottom', 'Time', units='sec')

    # make a legend on the top plot
    alegend = pg.LegendItem((80, 60), offset=(70, 20))
    curve_names = [f'Car{i}' for i in range(PLATOON_SIZE)]
    [alegend.addItem(curve, curve_name) for curve, curve_name in
     zip(aplot.curves, curve_names)]
    alegend.setParentItem(aplot)
    glw.nextRow()

    # velocity plot. Plot lead car with a different pen
    vplot: pg.PlotItem = glw.addPlot(title='Velocity', x=times,
                                     y=platoon_v[0], pen=lead_pen)
    [vplot.plot(x=times, y=v, pen=pen) for v, pen in
     zip(platoon_v[1:], follow_pens)]
    vplot.setLabel('left', 'Velocity', units='m/s')
    vplot.setLabel('bottom', 'Time', units='sec')
    glw.nextRow()

    # positions plot. Plot lead car with a different pen
    pplot: pg.PlotItem = glw.addPlot(title='Position', x=times,
                                     y=platoon_p[0], pen=lead_pen)
    [pplot.plot(x=times, y=p, pen=pen) for p, pen in
     zip(platoon_p[1:], follow_pens)]
    pplot.setLabel('left', 'Position', units='m')
    pplot.setLabel('bottom', 'Time', units='sec')
    glw.nextRow()

    # position errors plot. Plot lead car with a different pen
    errorplot: pg.PlotItem = glw.addPlot(title='Position Error')
    [errorplot.plot(x=times, y=perror, pen=pen) for perror, pen in
     zip(platoon_perror, follow_pens)]
    errorplot.setLabel('left', 'Position Error', units='m')
    errorplot.setLabel('bottom', 'Time', units='sec')
    glw.nextRow()

    # position change in errors plot. Plot lead car with a different pen
    errorplot: pg.PlotItem = glw.addPlot(title='Velocity Error')
    [errorplot.plot(x=times, y=verror, pen=pen) for verror, pen in
     zip(platoon_verror, follow_pens)]
    errorplot.setLabel('left', 'Velocity Error', units='m/s')
    errorplot.setLabel('bottom', 'Time', units='sec')

    win = QMainWindow()
    win.setWindowTitle('Manually-Tuned PID Plots')
    win.setCentralWidget(glw)
    win.showMaximized()
    win.setFocus()

    # start gui event loop so that plotting can happen
    pg.exec()
