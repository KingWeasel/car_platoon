import numpy as np


def triangle(x: float, a: float, b: float, c: float, ttype='center') -> float:
    if ttype == 'left':
        ret = np.fmax(np.fmin(1, (c - x) / (c - b)), 0)
    elif ttype == 'right':
        ret = np.fmax(np.fmin(1, (x - a) / (b - a)), 0)
    else:
        ret = np.fmax(np.fmin((c - x) / (c - b), (x - a) / (b - a)), 0)
    return ret


if __name__ == '__main__':
    error = -0.25
    derror = .01

    # scaling and coercing these to [-1, 1]
    scaled_error = np.clip(error, -1, 1)
    scaled_derror = np.clip(derror * 100, -1, 1)

    outMF_centers = np.array([-1, -2 / 3, -1 / 3, 0, 1 / 3, 2 / 3, 1])

    errNB = triangle(scaled_error, np.nan, -1, -2 / 3, ttype='left')
    errNM = triangle(scaled_error, -1, -2 / 3, -1 / 3)
    errNS = triangle(scaled_error, -2 / 3, -1 / 3, 0)
    errZE = triangle(scaled_error, -1 / 3, 0, 1 / 3)
    errPS = triangle(scaled_error, 0, 1 / 3, 2 / 3)
    errPM = triangle(scaled_error, 1 / 3, 2 / 3, 1)
    errPB = triangle(scaled_error, 2 / 3, 1, np.nan, ttype='right')
    errMF = np.array([[errNB, errNM, errNS, errZE, errPS, errPM, errPB]])

    derrNB = triangle(scaled_derror, np.nan, -1, -2 / 3, ttype='left')
    derrNM = triangle(scaled_derror, -1, -2 / 3, -1 / 3)
    derrNS = triangle(scaled_derror, -2 / 3, -1 / 3, 0)
    derrZE = triangle(scaled_derror, -1 / 3, 0, 1 / 3)
    derrPS = triangle(scaled_derror, 0, 1 / 3, 2 / 3)
    derrPM = triangle(scaled_derror, 1 / 3, 2 / 3, 1)
    derrPB = triangle(scaled_derror, 2 / 3, 1, np.nan, ttype='right')
    derrMF = np.array([[derrNB, derrNM, derrNS, derrZE, derrPS, derrPM, derrPB]])

    print(errMF.shape, derrMF.T.shape)
    twoDtable = np.matmul(errMF.T, derrMF)
    print(twoDtable)
