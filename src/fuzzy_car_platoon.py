from numba import njit
import numpy as np
from PyQt6.QtGui import QPen
from PyQt6.QtWidgets import QMainWindow
import pyqtgraph as pg
from simple_pid import PID

# constants, using metric units
TIME_LAG = 0.5  # System 'time lag' Tau defined in the paper's simulation
PLATOON_SIZE = 4
SAFE_DISTANCE = 2  # this is the same as Dmin defined in the paper
TARGET_TIME_GAP = 1.5  # 3 seconds seems like a good time gap to keep from a leading car
CAR_LENGTH = 5  # rounded down from the length of my Frontier
# CAR_MASS = 2e6
# g = 9.8
# F_ROLLING = 0.01
START_TIME = 0  # the paper went from t = 0..20 but plotted until 50
# STOP_TIME = 20
STOP_TIME = 50
TIME_STEP = 0.1

# manual PID parameters defined in paper
Kp = 2
Ki = 3
Kd = 0.5

NB = -1
NM = -2 / 3
NS = -1 / 3
ZE = 0
PS = 1 / 3
PM = 2 / 3
PB = 1

# could have made one 3D table, but this was easier for me to reason about
#                     NB  NM  NS  ZE  PS  PM  PB     <---ec
Kp_table = np.array([[PB, PM, PM, PM, PS, ZE, ZE],   # NB  e
                     [PB, PM, PM, PS, PS, ZE, NS],   # NM  |
                     [PM, PM, PM, PS, ZE, NS, NS],   # NS  v
                     [PM, PM, PS, ZE, NS, NM, NM],   # ZE
                     [PS, PS, ZE, NS, NS, NM, NM],   # PS
                     [PS, ZE, NS, NM, NM, NM, NB],   # PM
                     [ZE, ZE, NM, NM, NM, NM, NB]])  # PB

Ki_table = np.array([[NB, NB, NM, NM, NS, ZE, ZE],
                     [NB, NB, NM, NS, NS, ZE, ZE],
                     [NB, NM, NS, NS, ZE, PS, PS],
                     [NM, NM, NS, ZE, PS, PM, PM],
                     [NM, NS, ZE, PS, PS, PM, PB],
                     [ZE, ZE, PS, PS, PM, PB, PB],
                     [ZE, ZE, PS, PM, PM, PB, PB]])

Kd_table = np.array([[PS, NS, NB, NB, NB, NM, PS],
                     [PS, NS, NB, NM, NM, NS, ZE],
                     [ZE, NS, NM, NM, NS, NS, ZE],
                     [ZE, NS, NS, NS, NS, NS, ZE],
                     [ZE, ZE, ZE, ZE, ZE, ZE, ZE],
                     [PB, PS, PS, PS, PS, PS, PB],
                     [PB, PM, PM, PM, PS, PS, PB]])


@njit
def lead_positions(times: np.ndarray, velocities: np.ndarray,
                   p0: float = 0) -> np.ndarray:
    positions = np.zeros(len(times))
    first_t = times[0]
    last_t = first_t
    last_p = p0
    for i, (t, v) in enumerate(zip(times, velocities)):
        # current position is a function of the time delta, current velocity,
        # and last position
        positions[i] = ((t - last_t) * v) + last_p
        last_t = t
        last_p = positions[i]
    return positions


@njit
def lead_velocities(times: np.ndarray, accelerations: np.ndarray) -> np.ndarray:
    velocities = np.zeros(len(times))
    last_t = times[0]
    last_v = 0
    for i, (t, a) in enumerate(zip(times, accelerations)):
        # current velocity is a function of the time delta,
        # current acceleration, and last velocity
        velocities[i] = (t - last_t) * a + last_v
        last_t = t
        last_v = velocities[i]
    return velocities


@njit
def lead_car_dynamics(times: np.ndarray) -> tuple[np.ndarray]:
    # the paper says the acceleration is 4 m/s^2 if t is between 5 and 10 seconds
    lead_a = np.array([4 if (5 < t <= 10) else 0 for t in times])
    lead_v = lead_velocities(times, lead_a)
    lead_p = lead_positions(times, lead_v)
    return lead_a, lead_v, lead_p


# can't use numba because of PID
def follower_dynamics(times: np.ndarray, lead_positions: np.ndarray,
                      lead_velocities: np.ndarray, fuzzy: bool) -> tuple[np.ndarray]:

    # how many index positions to lag by to emulate a real time lag Tau
    index_lag = int(TIME_LAG / TIME_STEP)

    # car starts in sync with the car leading it with the correct time gap
    current_a = 0
    current_v = lead_velocities[index_lag]
    current_p = (lead_positions[index_lag] - CAR_LENGTH - SAFE_DISTANCE
                 - current_v * TARGET_TIME_GAP)

    len_times = len(times)

    # this function is going to return these after updating them based on
    # leading car dynamics
    follower_a = current_a * np.ones(len_times)
    follower_v = current_v * np.ones(len_times)
    follower_p = current_p * np.ones(len_times)

    pid = PID(Kp=Kp, Ki=Ki, Kd=Kd, setpoint=current_p)

    # This is how I'm interpreting "the control range is [-24, 24]"
    pid.output_limits = (-8, 8)
    # pid.output_limits = (-24, 24)

    # we'll let position be the front of the car (matches paper).
    # So then, position of a follower
    # car should be front car positition - constant time gap - car length
    error = 0
    for i, lp in enumerate(lead_positions[:-1 * index_lag], start=index_lag):

        target_p = lp - CAR_LENGTH - SAFE_DISTANCE - current_v * TARGET_TIME_GAP

        # the control part
        pid.setpoint = target_p
        current_a = pid(current_p, TIME_STEP)

        last_error = error
        error = target_p - current_p
        derror = error - last_error

        if fuzzy:
            dKp, dKi, dKd = fuzzy_coefficient_changes(error, derror)
            pid.Kp += dKp
            pid.Ki += dKi
            pid.Kd += dKd

        # collect values for return
        follower_a[i] = current_a
        # the scaling factor is my attempt to approximate momentum
        current_v += (current_a * TIME_STEP)
        follower_v[i] = current_v
        current_p += current_v * TIME_STEP
        follower_p[i] = current_p

    return follower_a, follower_v, follower_p


def fuzzy_coefficient_changes(error: float, derror: float) -> tuple[float]:

    # scaling and coercing these to [-1, 1]
    scaled_error = np.clip(error / 100, -1, 1)
    scaled_derror = np.clip(derror * 10, -1, 1)

    errNB = triangle(scaled_error, np.nan, -1, -2 / 3, ttype='L')
    errNM = triangle(scaled_error, -1, -2 / 3, -1 / 3)
    errNS = triangle(scaled_error, -2 / 3, -1 / 3, 0)
    errZE = triangle(scaled_error, -1 / 3, 0, 1 / 3)
    errPS = triangle(scaled_error, 0, 1 / 3, 2 / 3)
    errPM = triangle(scaled_error, 1 / 3, 2 / 3, 1)
    errPB = triangle(scaled_error, 2 / 3, 1, np.nan, ttype='R')
    # double brackets to make it 2D for transpose
    errMFs = np.array([[errNB, errNM, errNS, errZE, errPS, errPM, errPB]])

    derrNB = triangle(scaled_derror, np.nan, -1, -2 / 3, ttype='L')
    derrNM = triangle(scaled_derror, -1, -2 / 3, -1 / 3)
    derrNS = triangle(scaled_derror, -2 / 3, -1 / 3, 0)
    derrZE = triangle(scaled_derror, -1 / 3, 0, 1 / 3)
    derrPS = triangle(scaled_derror, 0, 1 / 3, 2 / 3)
    derrPM = triangle(scaled_derror, 1 / 3, 2 / 3, 1)
    derrPB = triangle(scaled_derror, 2 / 3, 1, np.nan, ttype='R')
    derrMFs = np.array([[derrNB, derrNM, derrNS, derrZE, derrPS, derrPM, derrPB]])

    # makes a 7x7 grid of the product of rule antecedents
    antecedent_products = np.matmul(errMFs.T, derrMFs)

    # denominator for SPIE with CA defuzzification
    denominator = np.sum(antecedent_products)

    # Kp part
    kp_numerator = np.sum(antecedent_products * Kp_table)
    # the multiply by 3 is to follow the paper's output scaling
    dKp = np.clip(3 * (kp_numerator / denominator), -3, 3)

    # Ki part
    ki_numerator = np.sum(antecedent_products * Ki_table)
    dKi = np.clip(3 * (ki_numerator / denominator), -3, 3)

    # Kd part
    kd_numerator = np.sum(antecedent_products * Kd_table)
    dKd = np.clip(3 * (kd_numerator / denominator), -3, 3)

    return dKp, dKi, dKd


@ njit
def triangle(x: float, a: float, b: float, c: float, ttype='C') -> float:
    if ttype == 'L':
        ret = np.fmax(np.fmin(1, (c - x) / (c - b)), 0)
    elif ttype == 'R':
        ret = np.fmax(np.fmin(1, (x - a) / (b - a)), 0)
    else:
        ret = np.fmax(np.fmin((c - x) / (c - b), (x - a) / (b - a)), 0)
    return ret


# START HERE
if __name__ == '__main__':
    times = np.arange(START_TIME, STOP_TIME, TIME_STEP)

    platoon_a = []
    platoon_v = []
    platoon_p = []
    platoon_verror = []

    # calculate dynamics for the entire platoon
    for i in range(3):
        if not i:
            # lead car
            (lead_a, lead_v, lead_p) = lead_car_dynamics(times)
            verror = None
            platoon_a.append(lead_a)
            platoon_v.append(lead_v)
            platoon_p.append(lead_p)
        else:
            # a following car, feed in dynamics from car in front of it
            (manual_a, manual_v, manual_p) = follower_dynamics(times, lead_p,
                                                               lead_v, fuzzy=False)
            (fuzzy_a, fuzzy_v, fuzzy_p) = follower_dynamics(times, lead_p,
                                                            lead_v, fuzzy=True)
            manual_verror = lead_v - manual_v
            fuzzy_verror = lead_v - fuzzy_v

            platoon_a.append(manual_a)
            platoon_v.append(manual_v)
            platoon_p.append(manual_p)
            platoon_verror.append(manual_verror)
            platoon_a.append(fuzzy_a)
            platoon_v.append(fuzzy_v)
            platoon_p.append(fuzzy_p)
            platoon_verror.append(fuzzy_verror)

    # plotting
    glw = pg.GraphicsLayoutWidget()

    colors = ('red', 'yellow', 'blue')
    lead_pen = pg.mkPen({'color': colors[0], 'width': 3, 'dash': (1, 2)})
    follow_pens: list[QPen] = [pg.mkPen({'color': color, 'width': 2})
                               for color in colors[1:]]

    # acceleration plot. Plot lead car with a different pen
    aplot: pg.PlotItem = glw.addPlot(title='Acceleration', x=times,
                                     y=platoon_a[0], pen=lead_pen)
    [aplot.plot(x=times, y=a, pen=pen) for a, pen in
     zip(platoon_a[1:], follow_pens)]
    aplot.setLabel('left', 'Acceleration', units='m/s^2')
    aplot.setLabel('bottom', 'Time', units='sec')

    # make a legend on the top plot
    alegend = pg.LegendItem((80, 60), offset=(70, 20))
    # curve_names = [f'Car{i}' for i in range(PLATOON_SIZE)]
    curve_names = ['Lead', 'Manual PID', 'Fuzzy PID']
    [alegend.addItem(curve, curve_name) for curve, curve_name in
     zip(aplot.curves, curve_names)]
    alegend.setParentItem(aplot)
    glw.nextRow()

    # velocity plot. Plot lead car with a different pen
    vplot: pg.PlotItem = glw.addPlot(title='Velocity', x=times,
                                     y=platoon_v[0], pen=lead_pen)
    [vplot.plot(x=times, y=v, pen=pen) for v, pen in
     zip(platoon_v[1:], follow_pens)]
    vplot.setLabel('left', 'Velocity', units='m/s')
    vplot.setLabel('bottom', 'Time', units='sec')
    glw.nextRow()

    # positions plot. Plot lead car with a different pen
    pplot: pg.PlotItem = glw.addPlot(title='Position', x=times,
                                     y=platoon_p[0], pen=lead_pen)
    [pplot.plot(x=times, y=p, pen=pen) for p, pen in
     zip(platoon_p[1:], follow_pens)]
    pplot.setLabel('left', 'Position', units='m')
    pplot.setLabel('bottom', 'Time', units='sec')
    glw.nextRow()

    # position change in errors plot. Plot lead car with a different pen
    errorplot: pg.PlotItem = glw.addPlot(title='Velocity Error')
    [errorplot.plot(x=times, y=verror, pen=pen) for verror, pen in
     zip(platoon_verror, follow_pens)]
    errorplot.setLabel('left', 'Velocity Error', units='m/s')
    errorplot.setLabel('bottom', 'Time', units='sec')

    win = QMainWindow()
    win.setWindowTitle('Fuzzy vs Manual PID Plots')
    win.setCentralWidget(glw)
    win.showMaximized()
    win.setFocus()

    # start gui event loop so that plotting can happen
    pg.exec()
